import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarNavComponent } from './components/sidebar-nav/sidebar-nav.component';
import { HeaderComponent } from './components/header/header.component';
import { MainContentComponent } from './components/main-content/main-content.component';
import { HomeComponent } from './components/main-content/home/home.component';
import { WhyUsComponent } from './components/main-content/why-us/why-us.component';
import { ServicesComponent } from './components/main-content/services/services.component';
import { ClientsComponent } from './components/main-content/clients/clients.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarNavComponent,
    HeaderComponent,
    MainContentComponent,
    HomeComponent,
    WhyUsComponent,
    ServicesComponent,
    ClientsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
